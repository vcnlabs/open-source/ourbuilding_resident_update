Function: To compare two CSV files, inform the user on how to update the OLD file to the NEW file.

Ignores entries without a phone.

unitUpdate.sh - A CSV File comparator for use in the wholesale replacement/updating of
 the OurBuilding Contact information spreadsheets

 Call -   bash unitUpdate.sh [oldFile.csv] [newFile.csv]
 Current implementation searches by phone number
 Ignores any entry that does not have a phone number.






Please test, mark issues in Issue Tracker if anything found.
I'll try to fix it when I get the chance. I'm moving on from VCN, but I'll do what I can.
-Warren Holley
WarrenGHolley@gmail.com
