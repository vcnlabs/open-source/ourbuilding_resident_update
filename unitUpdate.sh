#!/bin/bash

# unitUpdate.sh - A CSV File comparator for use in the big updating of
#  OurBuilding Contact information spreadsheets
# Call -   bash unitUpdate.sh [oldFile.csv] [newFile.csv]
# Current implementation searches by name, then alarms to update Unit#,
#Phone, Email, if needed.
#  If no name is found (null), search by phone#, update suite, email.
#  If neither name nor phone found, update by email.


#---------BUGS & TODO------------
# Implement output of raw phone numbers on CSV update/alarm alerts, so
#it's easier to find them.
# Maybe update format-checking exit condition to just remove the entries
#from the list?
#  Maybe not. User may delete new CSV file afterwards, unknowing of error.
#---------------------------------


#Database Output is a Comma-Seperated Values file.
# Unit#,Name,Phone#,Email Address

#NULL values are given as an empty string. (="")

#-----------------------
#-----FUNCTIONS---------
#-----------------------

#getPhoneNumber:
#Arg: CSV Entry String
#Output: Number String.
# Pulls the all digits out of CSV String.
# discards all non-digits (brackets, commas, dashes, letters, etc)
# Does not do error handling.
function getPhoneNumber() {
        numString=""
        unparsedNumber=$(echo "$1" | cut -d ',' -f3)
        for ((j=0; j<${#unparsedNumber};j++)); do
                if [[ ${unparsedNumber:j:1} =~ [0-9] ]]; then
                        numString+="${unparsedNumber:j:1}"
                fi
        done
        if [ "${numString:0:1}" == "1" ]; then
                numString=${numString:1}
        fi
        echo "$numString"
}
#makeReadable:
#Arg: CSV Entry String
#Output: CSV Entry String with phone number parsed to readable state.
# Calls getPhoneNumber
# Does not do error handling.
# For use of comparing old and new CSV entries, as phone number format is unfortunately varied.
function makeReadable() {
        readableNumberString=$(getPhoneNumber "$1")
        stringBuilder="$(echo "$1" | cut -d ',' -f1),$(echo "$1" | cut -d ',' -f2),$readableNumberString,$(echo "$1" | cut -d ',' -f4)"
        echo "$stringBuilder"
}

#arg: 
#Arguments: CSV Entry String, integer.
#Output: CS Value within the entry at index integer. (1-indexed)
# Returns String, or empty string for empty value.
# Cooperates with the 5-val format; 
function arg() {
	echo $(echo "$1" | cut -d ',' -f$2)
}

#Argument: CSV, potentially with marked value.
# Returns string without the marked/fifth value.
function scrubCSV() {
		echo $(echo $1 | cut -d ',' -f1-4)
}

#Echoes the 'marked' aspect of a CSV Entry, if it has one.
#Otherwise, echoes an empty string ""
function marked() {
	echo $(echo $1 | cut -d ',' -f5)
}

#Two args: A setup string, and a CSV Entry.
#Prints the setup string, then the CSV Entry, parsed as to have a space surrounding the phone number.
function printCSVwithSpace() {
	echo -n "$1 $(echo $2 | cut -d ',' -f1-2 ), "
	echo -n "$(arg $2 3)"
	echo " ,$(echo $2 | cut -d ',' -f4 )"
}

#-------------------------------
#------------SETUP--------------
#-------------------------------

oldCSVFile="$1"         #Test: testCasesOld.csv
newCSVFile="$2"         #Test: testCasesNew.csv

oldCSVArray=()
newCSVArray=()
oldRawArray=() #For more clairity when informing user. Not yet implemented in output (TODO)
newRawArray=() #For more clairity when informing user. Not yet implemented in output (TODO)


leavingTenants=()
newTenants=()
updatedTenants=()
movedTenants=()
unchangedTenants=()



#Check for correct number of args. Print Usage otherwise
if [ "$#" != "2" ]; then
		echo "unitUpdate.sh - A CSV File comparator for use in the wholesale replacement/updating of"
		echo " the OurBuilding Contact information spreadsheets" 
		echo "" 
		echo " Call -   bash unitUpdate.sh [oldFile.csv] [newFile.csv]"
		echo " Current implementation searches by phone number"
		echo " Ignores any entry that does not have a phone number."
        exit
fi


#Read from the files, push to arrays.
IFS=$'\n'
while read CSVEntry; do
        if [ "${CSVEntry}" == "BREAK" ]; then #Premature breaking. For testing purposes.
                break
        fi
        #Skip comment lines, empty csv entries, and empty strings.
        if [ "${CSVEntry:0:1}" != "#" ] && [ "${CSVEntry}" != ",,," ] && [ "${CSVEntry}" != "" ]; then
                oldCSVArray+=($(makeReadable "$CSVEntry"))
                oldRawArray+=("$CSVEntry")
        fi
done < "$oldCSVFile"

while read CSVEntry; do
        if [ "${CSVEntry}" == "BREAK" ]; then #Premature breaking. For testing purposes.
                break
        fi
        if [ "${CSVEntry:0:1}" != "#" ] && [ "${CSVEntry}" != ",,," ] && [ "${CSVEntry}" != "" ]; then
                newCSVArray+=($(makeReadable $CSVEntry))
                newRawArray+=("$CSVEntry")
        fi
done < "$newCSVFile"

#At this point, all entries are now parsed a nice, standardized format.
# Format:  Unit#,Name,Phone#,Email[,Flags]
# Types:   String,String,Parsed String,String[,String]
# Where the number is a 10+ digits. Ignores & discards leading '1'
# In code, catch and throw an error/notification if this phone# format is wrong
# Only have 4 args. 5th is added in the event of a marking, later in the script.

# $old/newRawArray is to let the User know the actual phone number to search for & change.
# Hopefully, over time, this will lend itself to a nice standardized format described above in the old CSV files.

#First: Check for old and new entries that do not comply to the standard. Alarm, skip in main set if found

newPhoneTooShort=()
oldPhoneTooShort=()
newEntryShortArgs=()
oldEntryShortArgs=()
haveNoPhoneNumbers=()
for ((i=0;i<${#newCSVArray[*]};i++)); do #Most of this is rendered moot. Scrub when available.
        newEntry="${newCSVArray[i]}"
        newUnit=$(arg  "${newCSVArray[i]}" 1)
        newName=$(arg  "${newCSVArray[i]}" 2)
        newPhone=$(arg  "${newCSVArray[i]}" 3)
        newEmail=$(arg  "${newCSVArray[i]}" 4)
                
        #Get count of arguments in each entry.
        count=0
        if [ "$newUnit" != "" ]; then
                count=$((count+1))
        fi
        if [ "$newName" != "" ]; then
                count=$((count+1))
        fi
        if [ "$newPhone" != "" ]; then
                count=$((count+2))
        fi
        if [ "$newEmail" != "" ]; then
                count=$((count+1))
        fi

        if (( $count < 2 )); then
                newEntryShortArgs+=("${newCSVArray[i]}")
                newCSVArray[$i]+=",Too Few Args"
        elif [ "$newPhone" != "" ]; then
                if ! [[ $newPhone =~ ^[0-9]{10,}$ ]]; then
                        newPhoneTooShort+=("${newCSVArray[i]}")
                        newCSVArray[$i]+=",Phone Too Short"
                fi
        else
			haveNoPhoneNumbers+=("${newCSVArray[i]}");
			newCSVArray[$i]+=",No Phone"
        fi
done #And do the same for the Old CSV Array
for ((i=0;i<${#oldCSVArray[*]};i++)); do
        newEntry="${oldCSVArray[i]}"
        newUnit=$(echo  "${oldCSVArray[i]}" | cut -d ',' -f1)
        newName=$(echo  "${oldCSVArray[i]}" | cut -d ',' -f2)
        newPhone=$(echo "${oldCSVArray[i]}" | cut -d ',' -f3)
        newEmail=$(echo "${oldCSVArray[i]}" | cut -d ',' -f4)

        #Get count of arguments in each entry.
        count=0
        if [ "$newUnit" != "" ]; then
                count=$((count+1))
        fi
        if [ "$newName" != "" ]; then
                count=$((count+1))
        fi
        if [ "$newPhone" != "" ]; then
                count=$((count+2))
        fi
        if [ "$newEmail" != "" ]; then
                count=$((count+1))
        fi

        if (( $count < 2 )); then
                oldEntryShortArgs+=("${oldCSVArray[i]}")
				oldCSVArray[$i]+=",Too Few Args."
        elif [ "$newPhone" != "" ]; then
                if ! [[ $newPhone =~ ^[0-9]{10,}$ ]]; then
                        oldPhoneTooShort+=("${oldCSVArray[i]}")
                        oldCSVArray[$i]+=",Phone Too Short"
                fi
        else
			haveNoPhoneNumbers+=("${oldCSVArray[i]}")
			oldCSVArray[$i]+=",Phone Too Short"
        fi
done

#If any errors in the lists:
if [ "${#newPhoneTooShort[*]}" != "0" ] || [ "${#newEntryShortArgs[*]}" != "0" ] || 
   [ "${#oldPhoneTooShort[*]}" != "0" ] || [ "${#oldEntryShortArgs[*]}" != "0" ]; then
   echo "ERROR LIST:"
   echo "-----------"
   
   if [ "${#newPhoneTooShort[*]}" != "0" ]; then
		echo "In New: Phone numbers too short. Need 10+ digits."
		for ((i=0;i<${#newPhoneTooShort[*]};i++)); do
			echo "  ${newPhoneTooShort[i]}"
		done
		echo ""
   fi
   if [ "${#newEntryShortArgs[*]}" != "0" ]; then
		echo "In New: Not enough Arguments."
		for ((i=0;i<${#newEntryShortArgs[*]};i++)); do
			echo "  ${newEntryShortArgs[i]}"
		done
		echo ""
   fi
   if [ "${#oldPhoneTooShort[*]}" != "0" ]; then
		echo "In Old: Phone numbers too short. Need 10+ digits."
		for ((i=0;i<${#oldPhoneTooShort[*]};i++)); do
			echo "  ${oldPhoneTooShort[i]}"
		done
		echo ""
   fi
   if [ "${#oldEntryShortArgs[*]}" != "0" ]; then
		echo "In Old: Not enough Arguments."
		for ((i=0;i<${#oldEntryShortArgs[*]};i++)); do
			echo "  ${oldEntryShortArgs[i]}"
		done
   fi
fi

#Double linebreak. Just for readability.
echo ""; echo ""
echo "EDIT INSTRUCTIONS START"
echo "-----------------------"

#At this point the parsed format should follow the standard for all entries.
# String,String,#{10+},String[,String]
# Where at most any two can be null.
# Where the final segement is an error code value, not to be printed.


#-------------------------------------
#-----------MAIN LOOP-----------------
#-------------------------------------


# In the old file, look for phones that do not have entries in the New file.
# This will mean a move-out.
# Also look for entries that match, but have different data.
#  This means a change in contact info.

for ((i=0;i<${#oldCSVArray[*]};i++)); do
	if [ "$(marked ${oldCSVArray[i]})" != "" ]; then
		continue
	fi
	
	match=""
	for ((j=0;j<${#newCSVArray[*]};j++)); do
		if [ "$(marked ${newCSVArray[j]})" != "" ]; then
			continue
		fi
		
		if [ "$(arg ${oldCSVArray[i]} 3)" == "$(arg ${newCSVArray[j]} 3)" ]; then
			match="true"
			break;
		fi	
	done
	if [ "$match" != "true" ]; then
		leavingTenants+=("${oldCSVArray[i]}")
		oldCSVArray[$i]="${oldCSVArray[i]},MoveOut"
	fi
done

#Look for move-ins. 
for ((i=0;i<${#newCSVArray[*]};i++)); do
	if [ "$(marked ${newCSVArray[i]})" != "" ]; then
		continue
	fi
	
	match=""
	for ((j=0;j<${#oldCSVArray[*]};j++)); do
		if [ "$(marked ${oldCSVArray[j]})" != "" ]; then
			continue
		fi
		if [ "$(arg ${newCSVArray[i]} 3)" == "$(arg ${oldCSVArray[j]} 3)" ]; then
			match="true"
			break;
		fi	
	done
	if [ "$match" != "true" ]; then
		newTenants+=("${newCSVArray[i]}")
		newCSVArray[$i]="${newCSVArray[i]},MoveIn"
	fi
done

#Look for updates:
for ((i=0;i<${#newCSVArray[*]};i++)); do
	if [ "$(marked ${oldCSVArray[i]})" != "" ]; then
		continue
	fi
	
	match=""
	for ((j=0;j<${#oldCSVArray[*]};j++)); do
		if [ "$(marked ${oldCSVArray[j]})" != "" ]; then
			continue
		fi
		
		if [ "$(arg ${newCSVArray[i]} 3)" == "$(arg ${oldCSVArray[j]} 3)" ]; then
			if [ "${newCSVArray[i]}" == "${oldCSVArray[j]}" ]; then
				break;
			fi
			updatedTenants+=("${oldCSVArray[j]}") #From Old
			updatedTenants+=("${newCSVArray[i]}") #To New
			newCSVArray[$i]="${newCSVArray[i]},Updated"
			oldCSVArray[$j]="${oldCSVArray[j]},Updated"
			break;
		fi	
	done
done








for ((i=0;i<${#leavingTenants[*]};i++)); do
	#echo "Remove: ${leavingTenants[i]}"
	printCSVwithSpace "Remove: " "${leavingTenants[i]}"
done
echo ""
echo ""
for ((i=0;i<${#newTenants[*]};i++)); do
	#echo "Add: ${newTenants[i]}"
	printCSVwithSpace "Add: " "${newTenants[i]}"
done
echo ""
echo ""
for ((i=0;i<${#updatedTenants[*]};i=i+2)); do
	#echo "Update: ${updatedTenants[i]}"
	#echo "    To: ${updatedTenants[i+1]}"
	printCSVwithSpace "Update: " "${updatedTenants[i]}"
	printCSVwithSpace "    To: " "${updatedTenants[i]}"
done

echo ""
echo ""
echo "And these users have no phone:"

for ((i=0;i<${#haveNoPhoneNumbers[*]};i++)); do
	#echo "  ${haveNoPhoneNumbers[i]}"
	printCSVwithSpace "" "${haveNoPhoneNumbers[i]}"
done



exit


