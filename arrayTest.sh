#!/bin/bash

IFS=$'\n'
while read CSVEntry; do
        if [ "${CSVEntry}" == "BREAK" ]; then #Premature breaking. For testing purposes.
                break
        fi
        #Skip comment lines, empty csv entries, and empty strings.
        if [ "${CSVEntry:0:1}" != "#" ] && [ "${CSVEntry}" != ",,," ] && [ "${CSVEntry}" != "" ]; then
                oldRawArray+=("$CSVEntry")
        fi
done < "$1"

IFS=$'\n' sortedEntry=($(sort -t, -k3 -k2 <<< "${oldRawArray[*]}"))
echo "${#oldRawArray[*]}"
echo "${#sortedEntry[*]}"

for ((i=0;i<10;i++)); do
echo "${sortedEntry[i]}"
done


sortedPhoneOld=($(sort -t, -k2 <<<"${oldCSVArray[*]}"))
